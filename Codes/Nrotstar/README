	      *************************************************
 	      *	Lorene codes for rotating relativistic stars  *
	      *************************************************

1/ Description of the various codes
   ================================

nrotstar:	Code for rigidly rotating stars in GR (quasi-isotropic gauge) 


2/ Compilation
   ===========

Typing

	make 

will produce the executable in a non-optimized version for debugging purposes.  
To get an optimized version for production purposes type instead

	make -f Makefile_O2

You can clear everything by

	make uninstall


3/ Input parameters
   ================

The code nrotstar needs two input files:

par_rot.d :  parameters of the computation
par_eos.d : description of the equation of state

Templates files are provided in the directory Parameters ("GR" stands for general 
relativity and "Newt" for Newtonian gravitation). For instance
  (i) to get the model displayed in Figs. 3.1 to 3.7 of http://arxiv.org/abs/1003.5015, do 
      cp Parameters/GR/APR_1.4Msol_716Hz/*.d .

  (ii)  to compute a relativistic polytropic (gamma=2) star rotating at the Keplerian limit, do
      cp Parameters/GR/Kepler/*.d .


4/ Running
   =======

Simply type

	nrotstar

Setting the parameter graph to 1 in the file par_rot.d ensures that some graphical outputs are produced at the end of the computation. 
The main characteristics of the computed star are summarized in the file result.txt (human readable format). 
The full final configuration is saved in the file resu.d (binary format). 
The metric part of it is also saved in the file resu_gyoto.d (binary format) in a format readable by 
the ray-tracing code GYOTO. 
Some output files like convergence.d or prof_*.d can be visualized via xmgrace.
Profiles of various fields are in the files prof_X.d where X is the name of the field. 
